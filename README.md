# tyba-test

## Description of project
Mobile application made in flutter for test of the company tyba.

This is an application that performs login, registration through a public api found in this blog:
- https://www.appsloveworld.com/sample-rest-api-url-for-testing-with-authentication/

It also consumes the google api to verify if the written city has nearby restaurants within a radius of 2000 meters.
Use the location of the phone and save in memory if there is a logged in user.

## Instructions
You must have Flutter installed in a version greater than or equal to 2.17.1.

Clone the repository and use the command:
 - flutter run
 
To run the application.

If you have any error in the compilation, remember to check in the official documentation that everything is correctly configured on your computer.
 - https://docs.flutter.dev/get-started/install
