import '../../../domain/repositories/authentication_repository.dart';
import '../local/authentication_client.dart';
import '../remote/authentication.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationAPI _api;
  final AuthenticationClient _client;
  AuthenticationRepositoryImpl(this._api, this._client);
  @override
  Future<Map<String, dynamic>?> login(String email, String password) async {
    await Future.delayed(const Duration(seconds: 1));
    return _api.login(email, password);
  }

  @override
  Future<String?> get accessToken => _client.accessToken;

  @override
  Future<void> setSession(String? token) {
    return _client.setSession(token.toString());
  }

  @override
  Future<String?> get accessData => _client.accessData;

  @override
  Future<void> setData(String? data) {
    return _client.setData(data.toString());
  }

  @override
  Future<void> logOut() {
    return _client.clear();
  }
}
