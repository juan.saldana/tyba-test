// ignore_for_file: constant_identifier_names

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const String TOKEN = "token";
const String DATA = "data";

class AuthenticationClient {
  final FlutterSecureStorage _storage;
  AuthenticationClient(this._storage);

  Future<String?> get accessToken {
    return _storage.read(key: TOKEN);
  }

  Future<void> setSession(String token) {
    return _storage.write(key: TOKEN, value: token);
  }

  Future<String?> get accessData {
    return _storage.read(key: DATA);
  }

  Future<void> setData(String data) {
    return _storage.write(key: DATA, value: data);
  }

  Future<void> clear() {
    return _storage.deleteAll();
  }
}
