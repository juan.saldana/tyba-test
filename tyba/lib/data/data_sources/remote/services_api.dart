// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:dio/dio.dart';

class ServicesAPI {
  final _dio = Dio(BaseOptions(baseUrl: 'http://restapi.adequateshop.com'));

  Future<Map<String, dynamic>?> register(
      String name, String email, String password) async {
    print(name);
    print(email);
    print(password);
    var formData =
        jsonEncode({'name': name, 'email': email, 'password': password});
    print(formData);

    try {
      final response = await _dio.post('/api/authaccount/registration',
          options: Options(headers: {"Content-Type": "application/json"}),
          data: formData);
      return response.data;
    } on DioError catch (e) {
      print('error register');
      print(e.message);
      return null;
    }
  }
}
