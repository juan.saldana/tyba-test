// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:flutter_meedu/meedu.dart';

import '../../../domain/repositories/authentication_repository.dart';

class MapsAPI {
  final _dio = Dio(BaseOptions(baseUrl: 'https://maps.googleapis.com/maps'));
  final _repository = Get.find<AuthenticationRepository>();

  Future<Map<String, dynamic>?> getCityFromInput(String city) async {
    final api = await _repository.accessToken;
    try {
      final response = await _dio.get('/api/place/findplacefromtext/json',
          queryParameters: {
            'input': city,
            'inputtype': 'textquery',
            'key': api
          });
      return response.data;
    } on DioError catch (e) {
      print('error city');
      print(e);
      return null;
    }
  }

  Future<Map<String, dynamic>?> getCityById(String placeId) async {
    final api = await _repository.accessToken;
    try {
      final response = await _dio.get('/api/place/details/json',
          queryParameters: {'place_id': placeId, 'key': api});
      return response.data;
    } on DioError catch (e) {
      print('error city');
      print(e);
      return null;
    }
  }

  Future<Map<String, dynamic>?> getPlaces(
      String location, String pagetoken) async {
    final api = await _repository.accessToken;
    try {
      final response =
          await _dio.get('/api/place/nearbysearch/json', queryParameters: {
        'location': location,
        'pagetoken': pagetoken,
        'radius': 2000,
        'type': 'restaurant',
        'key': api
      });
      return response.data;
    } on DioError catch (e) {
      print('error city');
      print(e);
      return null;
    }
  }
}
