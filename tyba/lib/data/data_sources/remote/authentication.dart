// ignore_for_file: avoid_print
import 'dart:convert';

import 'package:dio/dio.dart';

class AuthenticationAPI {
  final String _prod = 'http://apigateway.liwa.co:30002';
  String get prod => _prod;
  final String _test = 'http://52.11.216.50:8090';
  String get test => _test;

  final _dio = Dio(BaseOptions(baseUrl: 'http://restapi.adequateshop.com'));

  Future<Map<String, dynamic>?> login(String email, String password) async {
    var formData = jsonEncode({'email': email, 'password': password});

    try {
      final response = await _dio.post('/api/authaccount/login',
          options: Options(headers: {"Content-Type": "application/json"}),
          data: formData);
      return response.data;
    } on DioError catch (e) {
      print('error Authentication');
      print(e);
      return null;
    }
  }
}
