abstract class AuthenticationRepository {
  Future<String?> get accessToken;
  Future<void> setSession(String? token);
  Future<String?> get accessData;
  Future<void> setData(String? data);
  Future<Map<String, dynamic>?> login(String email, String password);
  Future<void> logOut();
}
