import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'data/data_sources/local/authentication_client.dart';
import 'data/data_sources/remote/authentication.dart';
import 'data/data_sources/repositories_impl/authentication_repository_impl.dart';
import 'domain/repositories/authentication_repository.dart';

void injectDependencies() {
  Get.lazyPut<AuthenticationRepository>(() => AuthenticationRepositoryImpl(
        AuthenticationAPI(),
        AuthenticationClient(const FlutterSecureStorage()),
      ));
}
