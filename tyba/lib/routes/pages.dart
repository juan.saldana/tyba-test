import 'package:flutter/material.dart';
import 'package:tyba/routes/routes.dart';
import 'package:tyba/ui/pages/history/history_page.dart';
import 'package:tyba/ui/pages/login/login_page.dart';
import 'package:tyba/ui/pages/register/register_page.dart';
import 'package:tyba/ui/pages/splash/splash_page.dart';

import '../ui/pages/home/home_page.dart';

Map<String, Widget Function(BuildContext)> appRoutes = {
  home: (_) => const HomePage(),
  login: (_) => const LoginPage(),
  register: (_) => const RegisterPage(),
  splash: (_) => const SplashPage(),
  history: (_) => const HistoryPage(),
};
