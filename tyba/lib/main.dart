import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:tyba/routes/pages.dart';
import 'package:tyba/routes/routes.dart';

import 'dependency_injection.dart';

Future<void> main() async {
  injectDependencies();
  await dotenv.load(fileName: ".env");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData();
    return MaterialApp(
      theme: theme.copyWith(
        colorScheme: theme.colorScheme.copyWith(
          primary: const Color.fromRGBO(43, 150, 54, 1),
        ),
      ),
      navigatorKey: router.navigatorKey,
      navigatorObservers: [router.observer],
      debugShowCheckedModeBanner: false,
      routes: appRoutes,
      initialRoute: splash,
    );
  }
}
