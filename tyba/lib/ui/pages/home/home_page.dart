import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tyba/routes/routes.dart';
import 'package:tyba/ui/pages/history/history_page.dart';

import '../../../domain/repositories/authentication_repository.dart';
import '../widgets/loading_widget.dart';
import 'controller/home_controller.dart';

final homeProvider =
    SimpleProvider((_) => HomeController(), autoDispose: false);

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).size.height * 0.15;
    final repository = Get.find<AuthenticationRepository>();
    RefreshController refreshController =
        RefreshController(initialRefresh: false);

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.history),
          onPressed: () {
            historyProvider.read.chargeData();
            Navigator.pushNamed(context, history);
          },
        ),
        title: const Text('Tyba Test'),
        actions: [
          IconButton(
            onPressed: () async {
              await repository.logOut();
              homeProvider.read.clear();
              homeProvider.read.controllerSearch.clear();
              // ignore: use_build_context_synchronously
              Navigator.pushNamedAndRemoveUntil(context, login, (_) => false);
            },
            icon: const Icon(
              Icons.power_settings_new_rounded,
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Center(child: Consumer(
            builder: (context, ref, child) {
              final controllerHome = ref.watch(homeProvider);
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                    width: width * 0.9,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          height: height * 0.08,
                          width: width * 0.5,
                          child: TextField(
                            enabled: !controllerHome.isLocation,
                            controller: controllerHome.controllerSearch,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Buscar lugar',
                            ),
                          ),
                        ),
                        ElevatedButton(
                            onPressed: () {
                              if (controllerHome.isLocation) {
                                controllerHome.updateIsLocation(false);
                                controllerHome.listPlaces.clear();
                              } else {
                                controllerHome.updateIsLocation(true);
                                controllerHome.controllerSearch.text = '';
                                controllerHome.listPlaces.clear();
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: controllerHome.isLocation
                                  ? const Color.fromRGBO(43, 150, 54, 1)
                                  : const Color.fromRGBO(43, 150, 54, 200),
                            ),
                            child: SizedBox(
                                height: height * 0.08,
                                child: const Center(child: Icon(Icons.map)))),
                        ElevatedButton(
                            onPressed: () async {
                              if (controllerHome.isLocation) {
                                final response = await _determinePosition();
                                controllerHome.searchPlaceLocation(
                                    response.latitude.toString(),
                                    response.longitude.toString());
                              } else {
                                if (controllerHome
                                    .controllerSearch.text.isEmpty) {
                                  controllerHome.clear();
                                } else {
                                  controllerHome.searchPlace();
                                }
                              }
                            },
                            child: SizedBox(
                                height: height * 0.08,
                                child: const Center(child: Text('Buscar'))))
                      ],
                    ),
                  ),
                  Flexible(
                    child: SmartRefresher(
                      controller: refreshController,
                      enablePullDown: false,
                      enablePullUp: true,
                      footer: CustomFooter(
                        builder: (BuildContext context, LoadStatus? mode) {
                          Widget body;
                          if (mode == LoadStatus.idle) {
                            body = const Text('');
                          } else if (mode == LoadStatus.loading) {
                            body = const CupertinoActivityIndicator();
                          } else if (mode == LoadStatus.failed) {
                            body = const Text('');
                          } else if (mode == LoadStatus.canLoading) {
                            body = const Text('');
                          } else {
                            body = const Text('');
                          }
                          return SizedBox(
                            height: 55.0,
                            child: Center(child: body),
                          );
                        },
                      ),
                      onLoading: () async {
                        // ignore: unnecessary_null_comparison
                        if (controllerHome.nextPage != null) {
                          final places = await controllerHome.getPlaces(
                              controllerHome.location, controllerHome.nextPage);
                          controllerHome.updateListPlaces(places!['results']);
                          controllerHome
                              .updateNextPage(places['next_page_token']);
                        }

                        refreshController.loadComplete();
                      },
                      child: controllerHome.listPlaces.isEmpty
                          ? Center(
                              child: Text(controllerHome.isLoading
                                  ? ''
                                  : 'Sin Resultados'))
                          : ListView.builder(
                              itemBuilder: (context, index) {
                                final listPlaces = controllerHome.listPlaces;

                                return Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: height * 0.02),
                                  margin: EdgeInsets.only(top: height * 0.01),
                                  child: Card(
                                    elevation: 8,
                                    child: Column(
                                      children: [
                                        ListTile(
                                          title:
                                              Text(listPlaces[index]['name']),
                                          subtitle: Text(listPlaces[index]
                                                      ['vicinity'] ==
                                                  ''
                                              ? 'Sin direccion'
                                              : listPlaces[index]['vicinity']),
                                          leading: SizedBox(
                                            width: width * 0.15,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                const Icon(
                                                  Icons.star,
                                                  color: Colors.yellow,
                                                ),
                                                Text(listPlaces[index]
                                                            ['rating'] ==
                                                        null
                                                    ? 'No'
                                                    : listPlaces[index]
                                                            ['rating']
                                                        .toString()),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              itemCount: controllerHome.listPlaces.length,
                            ),
                    ),
                  ),
                ],
              );
            },
          )),
          Consumer(
            builder: (_, ref, __) {
              final controllerConsumer = ref.watch(homeProvider);
              return controllerConsumer.isLoading
                  ? const Loading()
                  : Container();
            },
          ),
        ],
      ),
    );
  }
}

Future<Position> _determinePosition() async {
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  return await Geolocator.getCurrentPosition();
}
