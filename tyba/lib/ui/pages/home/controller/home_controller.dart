import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:tyba/data/data_sources/remote/maps_api.dart';

import '../../../../domain/repositories/authentication_repository.dart';

class HomeController extends SimpleNotifier {
  final mapsApi = MapsAPI();
  final _repository = Get.find<AuthenticationRepository>();

  final TextEditingController _controllerSearch = TextEditingController();
  TextEditingController get controllerSearch => _controllerSearch;

  final List<dynamic> _listPlaces = [];
  List<dynamic> get listPlaces => _listPlaces;
  void updateListPlaces(List<dynamic> places) {
    for (var item in places) {
      _listPlaces.add(item);
    }
    notify();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  void updateIsLoading(bool value) {
    _isLoading = value;
    notify();
  }

  bool _isLocation = false;
  bool get isLocation => _isLocation;
  void updateIsLocation(bool value) {
    _isLocation = value;
    notify();
  }

  void clear() {
    _listPlaces.clear();
    notify();
  }

  String _location = '';
  String get location => _location;

  void updateLocation(String text) {
    _location = text;
    notify();
  }

  String _nextPage = '';
  String get nextPage => _nextPage;

  void updateNextPage(String text) {
    _nextPage = text;
    notify();
  }

  Future<void> updateData(String name) async {
    final data = await _repository.accessData;
    if (data == null) {
      List initialData = [
        {'name': name, 'time': DateTime.now().toIso8601String()}
      ];
      await _repository.setData(jsonEncode(initialData));
    } else {
      final dataStorage = jsonDecode(data);
      dataStorage.add({'name': name, 'time': DateTime.now().toIso8601String()});
      await _repository.setData(jsonEncode(dataStorage));
    }
  }

  Future<void> searchPlace() async {
    updateIsLoading(true);
    updateData(controllerSearch.text);
    final placeId = await getCityFromInput(controllerSearch.text);
    final response = await getCityById(placeId!['candidates'][0]['place_id']);
    final location = response!['result']['geometry']['location'];
    final locationEndpoint = '${location['lat']}, ${location['lng']}';
    final places = await getPlaces(locationEndpoint, '');
    listPlaces.clear();
    updateListPlaces(places!['results']);
    updateLocation(locationEndpoint);
    updateNextPage(places['next_page_token']);
    updateIsLoading(false);
  }

  Future<void> searchPlaceLocation(String lat, String long) async {
    updateIsLoading(true);
    final locationEndpoint = '$lat, $long';
    final places = await getPlaces(locationEndpoint, '');
    listPlaces.clear();
    updateListPlaces(places!['results']);
    updateLocation(locationEndpoint);
    updateNextPage(places['next_page_token']);
    updateIsLoading(false);
  }

  Future<Map<String, dynamic>?> getCityFromInput(String city) async {
    final response = await mapsApi.getCityFromInput(city);
    return response;
  }

  Future<Map<String, dynamic>?> getCityById(String id) async {
    final response = await mapsApi.getCityById(id);
    return response;
  }

  Future<Map<String, dynamic>?> getPlaces(
      String location, String pageToken) async {
    final response = await mapsApi.getPlaces(location, pageToken);
    return response;
  }
}
