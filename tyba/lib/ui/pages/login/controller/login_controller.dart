// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_meedu/meedu.dart';

import '../../../../domain/repositories/authentication_repository.dart';
import '../login_state.dart';

class LoginController extends StateNotifier<LoginState> {
  GlobalKey<FormState> formKey = GlobalKey();
  final _repository = Get.find<AuthenticationRepository>();
  String api = dotenv.get('API_KEY');

  LoginController() : super(LoginState.initialState);

  bool _passwordExist = false;
  bool get passwordExist => _passwordExist;

  bool _isView = true;
  bool get isView => _isView;
  void updateIsView(bool condition) {
    _isView = condition;
    notifyListeners(state);
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  void updateIsLoading(bool value) {
    _isLoading = value;
    notifyListeners(state);
  }

  void onEmailChanged(String text) {
    state = state.copyWith(email: text);
  }

  void onPasswordChanged(String text) {
    state = state.copyWith(password: text);
    if (text != '') {
      _passwordExist = true;
    } else {
      _passwordExist = false;
    }
  }

  Future<bool> submit() async {
    final response = await _repository.login(state.email, state.password);
    if (response!['code'] == 0) {
      _repository.setSession(api);
      return true;
    }
    return false;
  }

  Future<void> showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: const Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Usuario o contraseña incorrectos'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'De acuerdo',
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
