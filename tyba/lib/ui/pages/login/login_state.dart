import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  final String email, password;

  const LoginState({
    required this.email,
    required this.password,
  });

  static LoginState get initialState =>
      const LoginState(email: '', password: '');

  LoginState copyWith({String? email, String? password}) {
    return LoginState(
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  @override
  List<Object?> get props => [email, password];
}
