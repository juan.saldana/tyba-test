import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:tyba/routes/routes.dart';
import 'package:tyba/ui/pages/login/login_state.dart';
import '../widgets/loading_widget.dart';
import 'controller/login_controller.dart';

final loginProvider = StateProvider<LoginController, LoginState>(
  (ref) => LoginController(),
);

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final controller = loginProvider.read;

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          body: Stack(
        children: [
          SingleChildScrollView(
            child: Form(
              key: controller.formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Center(
                    child: SizedBox(
                      width: width * 0.4,
                      height: height * 0.22,
                      child: Image.network(
                        'https://tyba.com.co/wp-content/uploads/2021/02/tyba-logo.png',
                        width: width,
                      ),
                    ),
                  ),
                  Container(
                    width: width * 0.8,
                    height: height * 0.07,
                    padding: EdgeInsets.only(left: width * 0.07),
                    margin: EdgeInsets.only(
                      top: height * 0.03,
                    ),
                    decoration: shadow(),
                    child: Center(
                      child: TextFormField(
                        autocorrect: false,
                        keyboardType: TextInputType.visiblePassword,
                        onChanged: loginProvider.read.onEmailChanged,
                        style: TextStyle(
                          color: const Color.fromRGBO(150, 150, 150, 1),
                          fontFamily: 'Lato',
                          fontSize: height * 0.02,
                        ),
                        decoration: decorationInput(height,
                            'Correo electrónico', false, loginProvider.read),
                        validator: (text) {
                          if (text!.trim().isNotEmpty) {
                            return null;
                          }
                          return "Introduzca email válido";
                        },
                      ),
                    ),
                  ),
                  Container(
                    width: width * 0.8,
                    height: height * 0.07,
                    padding: EdgeInsets.only(left: width * 0.07),
                    margin: EdgeInsets.only(
                      top: height * 0.02,
                    ),
                    decoration: shadow(),
                    child: Center(
                      child: Consumer(
                        builder: (_, ref, __) {
                          final controllerLogin = ref.watch(loginProvider);
                          return TextFormField(
                            onChanged: controllerLogin.onPasswordChanged,
                            style: TextStyle(
                              color: const Color.fromRGBO(150, 150, 150, 1),
                              fontFamily: 'Lato',
                              fontSize: height * 0.02,
                            ),
                            obscureText: controllerLogin.isView,
                            decoration: decorationInput(
                                height, 'Contraseña', true, controllerLogin),
                            validator: (text) {
                              if (text!.trim().isNotEmpty) {
                                return null;
                              }
                              return "Introduzca una contraseña";
                            },
                          );
                        },
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: width * 0.8,
                    height: height * 0.06,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, register);
                      },
                      child: Text(
                        '¿No tienes una cuenta? Registrarme',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: height * 0.018,
                            fontFamily: 'Lato',
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: height * 0.05),
                    width: width * 0.6,
                    height: height * 0.06,
                    child: ElevatedButton(
                      onPressed: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _submit(context);
                      },
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(width * 0.04),
                        ),
                      ),
                      child: Text(
                        'Iniciar Sesión',
                        style: TextStyle(
                            fontSize: height * 0.023, fontFamily: 'Lato'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Consumer(
            builder: (_, ref, __) {
              final controllerConsumer = ref.watch(loginProvider);
              return controllerConsumer.isLoading
                  ? const Loading()
                  : Container();
            },
          ),
        ],
      )),
    );
  }

  InputDecoration decorationInput(
      double height, String hintText, bool condition, controller) {
    return InputDecoration(
      errorStyle: const TextStyle(height: 0.045, fontFamily: 'Lato'),
      suffixIcon: condition && controller.passwordExist
          ? GestureDetector(
              child: Icon(
                  controller.isView ? Icons.visibility_off : Icons.visibility),
              onTap: () {
                if (controller.isView) {
                  controller.updateIsView(false);
                } else {
                  controller.updateIsView(true);
                }
              },
            )
          : null,
      focusColor: Colors.red,
      hoverColor: Colors.red,
      border: InputBorder.none,
      fillColor: Colors.black,
      hintStyle: TextStyle(
        color: const Color.fromRGBO(150, 150, 150, 50),
        fontFamily: 'Lato',
        fontSize: height * 0.02,
      ),
      hintText: hintText,
    );
  }

  BoxDecoration shadow() {
    return BoxDecoration(boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.5),
        spreadRadius: 0.1,
        blurRadius: 5,
        offset: const Offset(-1, 3),
      ),
    ], color: Colors.white, borderRadius: BorderRadius.circular(10));
  }
}

Future<void> _submit(context) async {
  final controller = loginProvider.read;
  final isOk = controller.formKey.currentState?.validate();
  controller.updateIsLoading(true);
  if (isOk!) {
    final isLogged = await controller.submit();
    if (isLogged) {
      controller.updateIsLoading(false);

      Navigator.pushNamedAndRemoveUntil(context, home, (_) => false);
    } else {
      controller.updateIsLoading(false);

      controller.showMyDialog(context);
    }
  }
}
