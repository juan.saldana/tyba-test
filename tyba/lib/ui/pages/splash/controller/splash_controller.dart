// ignore_for_file: prefer_final_fields, prefer_typing_uninitialized_variables
import 'package:flutter_meedu/meedu.dart';
import 'package:tyba/routes/routes.dart';

import '../../../../domain/repositories/authentication_repository.dart';

class SplashController extends SimpleNotifier {
  SplashController() {
    _init();
  }

  final _repository = Get.find<AuthenticationRepository>();
  String? _routeName;
  String? get routeName => _routeName;

  Future<void> _init() async {
    bool isLogged = await _repository.accessToken != null;

    if (isLogged) {
      _routeName = home;
    } else {
      _routeName = login;
    }
    notify();
  }
}
