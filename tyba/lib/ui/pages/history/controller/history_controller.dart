import 'dart:convert';

import 'package:flutter_meedu/meedu.dart';

import '../../../../domain/repositories/authentication_repository.dart';

class HistoryController extends SimpleNotifier {
  final _repository = Get.find<AuthenticationRepository>();

  List<dynamic> _listCities = [];
  List<dynamic> get listCities => _listCities;
  void updateListCities(List<dynamic> cities) {
    _listCities = cities;
    notify();
  }

  Future<void> chargeData() async {
    final data = await _repository.accessData;
    if (data != null) {
      final dataStorage = jsonDecode(data);
      updateListCities(dataStorage);
    }
    notify();
  }
}
