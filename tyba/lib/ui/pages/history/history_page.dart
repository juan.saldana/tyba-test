import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'controller/history_controller.dart';

final historyProvider =
    SimpleProvider((_) => HistoryController(), autoDispose: false);

class HistoryPage extends StatelessWidget {
  const HistoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).size.height * 0.15;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.keyboard_arrow_left),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: const Text('Tyba Test'),
      ),
      body: Stack(
        children: [
          Center(
            child: historyProvider.read.listCities.isNotEmpty
                ? Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                    width: width * 0.8,
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return Card(
                          elevation: 8,
                          child: ListTile(
                            title: Text(
                                historyProvider.read.listCities[index]['name']),
                            subtitle: Text(
                                historyProvider.read.listCities[index]['time']),
                          ),
                        );
                      },
                      itemCount: historyProvider.read.listCities.length,
                    ),
                  )
                : const Text('No hay datos'),
          )
        ],
      ),
    );
  }
}
