// ignore_for_file: avoid_print

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';

import '../../../../data/data_sources/remote/services_api.dart';
import '../register_state.dart';

class RegisterController extends StateNotifier<RegisterState> {
  GlobalKey<FormState> formKey = GlobalKey();
  final _repository = ServicesAPI();

  RegisterController() : super(RegisterState.initialState);

  void onEmailChanged(String text) {
    state = state.copyWith(email: text);
  }

  void onNameChanged(String text) {
    state = state.copyWith(name: text);
  }

  void onPasswordChanged(String text) {
    state = state.copyWith(password: text);
  }

  Future<bool> submit() async {
    final response =
        await _repository.register(state.name, state.email, state.password);
    print(response);
    if (response!['code'] == 0) {
      return true;
    }
    return false;
  }

  Future<void> popUp(BuildContext context, bool condition, height, width) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: SizedBox(
            height: height * 0.3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CircleAvatar(
                    radius: height * 0.06,
                    backgroundColor: Colors.white,
                    child: Icon(
                      condition ? Icons.check : Icons.close,
                      size: width * 0.2,
                      color: const Color.fromRGBO(43, 150, 54, 1),
                    )),
                Center(
                  child: Text(
                    condition ? 'Registro Exitoso' : 'Error',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontFamily: 'Lato', fontWeight: FontWeight.bold),
                  ),
                ),
                Center(
                  child: Text(
                    condition
                        ? 'Ya puedes iniciar sesión'
                        : 'Es probable que el correo ya se haya utilizado para el registro',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontFamily: 'Lato',
                    ),
                  ),
                ),
                SizedBox(
                  width: width * 0.6,
                  height: height * 0.05,
                  child: ElevatedButton(
                    onPressed: condition
                        ? () {
                            Navigator.pop(context);
                            Navigator.pop(context);
                          }
                        : () {
                            Navigator.pop(context);
                          },
                    style: ElevatedButton.styleFrom(
                      primary: const Color.fromRGBO(43, 150, 54, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.04),
                      ),
                    ),
                    child: Text(
                      condition ? 'Continuar' : 'Intentar de nuevo',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: height * 0.022,
                          fontFamily: 'Lato'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
