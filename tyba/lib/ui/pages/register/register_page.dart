import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';
import 'package:tyba/ui/pages/register/register_state.dart';

import 'controller/register_controller.dart';

final registerProvider = StateProvider<RegisterController, RegisterState>(
    (ref) => RegisterController(),
    autoDispose: false);

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final controller = registerProvider.read;
    return Scaffold(
        body: Stack(
      children: [
        Form(
          key: controller.formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width: width * 0.3,
                  height: height * 0.15,
                  alignment: Alignment.bottomCenter,
                  child: Image.network(
                    'https://tyba.com.co/wp-content/uploads/2021/02/tyba-logo.png',
                    width: width,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin:
                    EdgeInsets.only(bottom: height * 0.01, top: height * 0.02),
                width: width * 0.8,
                child: Text(
                  'Registrate',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: height * 0.02,
                    fontFamily: 'Lato',
                  ),
                ),
              ),
              Consumer(builder: ((context, ref, child) {
                final controllerRegister = ref.watch(registerProvider);
                return SizedBox(
                    height: height * 0.5,
                    child: SingleChildScrollView(
                        child: Column(
                      children: [
                        data1(width, height, controllerRegister, context),
                        Container(
                          margin: EdgeInsets.only(top: height * 0.02),
                          width: width * 0.6,
                          height: height * 0.05,
                          child: ElevatedButton(
                            onPressed: () async {
                              _submit(context, width, height);
                            },
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(width * 0.04),
                              ),
                            ),
                            child: Text(
                              'Registrarme',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: height * 0.022,
                                  fontFamily: 'Lato'),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: height * 0.02),
                          child: RichText(
                            text: TextSpan(
                              text: '¿Tienes una cuenta? ',
                              style: const TextStyle(
                                color: Color.fromRGBO(133, 133, 133, 1),
                                fontFamily: 'Lato',
                              ),
                              children: [
                                WidgetSpan(
                                    child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text(' Iniciar Sesión',
                                      style: TextStyle(
                                          color: Color.fromRGBO(43, 150, 54, 1),
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.bold)),
                                )),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )));
              }))
            ],
          ),
        ),
      ],
    ));
  }

  Column data1(double width, double height, RegisterController controller,
      BuildContext context) {
    return Column(
      children: [
        Container(
          width: width * 0.8,
          height: height * 0.05,
          padding: EdgeInsets.only(left: width * 0.07),
          margin: EdgeInsets.only(
            top: height * 0.02,
          ),
          decoration: shadow(),
          child: Center(
            child: TextFormField(
              onChanged: registerProvider.read.onNameChanged,
              style: const TextStyle(color: Colors.black, fontFamily: 'Lato'),
              decoration:
                  decorationInput(height, 'Nombres', controller, context),
              validator: (text) {
                if (text!.trim().isNotEmpty) {
                  return null;
                }

                return "Introduce nombre válido";
              },
            ),
          ),
        ),
        Container(
          width: width * 0.8,
          height: height * 0.05,
          padding: EdgeInsets.only(left: width * 0.07),
          margin: EdgeInsets.only(
            top: height * 0.02,
          ),
          decoration: shadow(),
          child: Center(
            child: TextFormField(
              onChanged: registerProvider.read.onEmailChanged,
              style: const TextStyle(color: Colors.black, fontFamily: 'Lato'),
              decoration: decorationInput(
                  height, 'Correo electrónico', controller, context),
              validator: (text) {
                if (text!.trim().isNotEmpty &&
                    text.contains('@') &&
                    text.contains('.')) {
                  return null;
                }

                return "Introduce un email válido";
              },
            ),
          ),
        ),
        Consumer(builder: (_, ref, __) {
          return Column(
            children: [
              Container(
                width: width * 0.8,
                height: height * 0.05,
                padding: EdgeInsets.only(left: width * 0.07),
                margin: EdgeInsets.only(
                  top: height * 0.02,
                ),
                decoration: shadow(),
                child: Center(
                  child: TextFormField(
                    obscureText: true,
                    onChanged: registerProvider.read.onPasswordChanged,
                    style: const TextStyle(
                        color: Colors.black, fontFamily: 'Lato'),
                    decoration: decorationInput(
                        height, 'Contraseña', controller, context),
                    validator: (text) {
                      if (text!.trim().isNotEmpty && text.length > 6) {
                        return null;
                      }
                      return "Contraseña debe ser mayor de 6 caracteres";
                    },
                  ),
                ),
              ),
            ],
          );
        }),
      ],
    );
  }

  InputDecoration decorationInput(
      double height, String hintText, controller, context) {
    return InputDecoration(
      errorStyle: const TextStyle(height: 0.045, fontFamily: 'Lato'),
      focusColor: Colors.red,
      hoverColor: Colors.red,
      border: InputBorder.none,
      fillColor: Colors.black,
      counterText: '',
      hintStyle: TextStyle(
        color: const Color.fromRGBO(150, 150, 150, 50),
        fontFamily: 'Lato',
        fontSize: height * 0.02,
      ),
      hintText: hintText,
    );
  }

  BoxDecoration shadow() {
    return BoxDecoration(boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.5),
        spreadRadius: 0.1,
        blurRadius: 5,
        offset: const Offset(-1, 3), // changes position of shadow
      ),
    ], color: Colors.white, borderRadius: BorderRadius.circular(10));
  }
}

Future<void> _submit(context, width, height) async {
  final controller = registerProvider.read;
  final isOk = controller.formKey.currentState?.validate();
  if (isOk!) {
    final isRegister = await controller.submit();
    if (isRegister) {
      controller.popUp(context, true, height, width);
    } else {
      controller.popUp(context, false, height, width);
    }
  }
}
