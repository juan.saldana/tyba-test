import 'package:equatable/equatable.dart';

class RegisterState extends Equatable {
  final String email, name, password;

  const RegisterState({
    required this.name,
    required this.email,
    required this.password,
  });

  static RegisterState get initialState => const RegisterState(
        name: '',
        email: '',
        password: '',
      );

  RegisterState copyWith({
    String? name,
    String? email,
    String? password,
  }) {
    return RegisterState(
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  @override
  List<Object?> get props => [name, email, password];
}
